export default {
  isFetching: (state) => {
    return state.isFetching
  },
  station: (state) => (id) => {
    return state.data.find(station => station.indicativo === id)
  },
  cities: (state) => {
    const ret = {}
    state.data.forEach(station => {
      if (typeof ret[station.provincia] === 'undefined') {
        ret[station.provincia] = {
          name: station.provincia,
          stations: []
        }
      }
      ret[station.provincia].stations.push(station)
    })
    return Object.values(ret)
  }
}
