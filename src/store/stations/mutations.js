import * as type from './types'

export default {
  [type.REQUEST_STATIONS] (state) {
    state.isFetching = true
    state.error = null
  },

  [type.REQUEST_STATIONS_SUCCESS] (state, action) {
    state.isFetching = false
    state.data = action.payload
    state.error = null
  },

  [type.REQUEST_STATIONS_ERROR] (state, action) {
    state.isFetching = false
    state.error = action.error
  },

  [type.CACHE_HIT_STATIONS] (state) {
    // TODO Cache-Expire
  }
}
