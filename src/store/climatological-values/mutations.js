import * as type from './types'

export default {
  [type.REQUEST_CLIMATOLOGICAL_VALUES] (state) {
    state.isFetching = true
    state.error = null
  },

  [type.REQUEST_CLIMATOLOGICAL_VALUES_SUCCESS] (state, action) {
    state.isFetching = false
    state.data = action.payload
    state.error = null
  },

  [type.REQUEST_CLIMATOLOGICAL_VALUES_ERROR] (state, action) {
    state.isFetching = false
    state.error = action.error
  }
}
