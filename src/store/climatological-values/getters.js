export default {
  isFetching: (state) => {
    return state.isFetching
  },
  climatologicalValues: (state) => {
    return state.data
  }
}
