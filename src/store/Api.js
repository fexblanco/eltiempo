import axios from 'axios'

export default axios.create({
  baseURL: process.env.AEMET_API_URL,
  withCredentials: true,
  params: {
    api_key: process.env.AEMET_API_KEY
  },
  headers: {
    'Content-Type': 'application/json'
  }
})
