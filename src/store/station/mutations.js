import * as type from './types'

export default {
  [type.REQUEST_STATION] (state) {
    state.isFetching = true
    state.error = null
  },

  [type.REQUEST_STATION_SUCCESS] (state, action) {
    state.isFetching = false
    state.data = action.payload
    state.error = null
  },

  [type.REQUEST_STATION_ERROR] (state, action) {
    state.isFetching = false
    state.error = action.error
  },

  [type.CACHE_HIT_STATION] (state, action) {
    state.data = action.payload
    // TODO Cache-Expire
  }
}
